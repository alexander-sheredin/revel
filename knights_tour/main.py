__author__ = 'Alexander Sheredin'

"""
Algorithm success is guarantied
for N x N boards with N%8 == 0
"""
board_size = 8

# sequence based on Douglas Squirrel's algorithm
# https://www.cs.cmu.edu/~sganzfri/Knights_REU04.pdf
# http://math.oregonstate.edu/~math_reu/proceedings/REU_Proceedings
# /Proceedings1996/1996Squirrel.pdf
ordered_moves = ((1, -2), (2, -1), (2, 1), (1, 2), (-1, 2), (-2, 1), (-2, -1), (-1, -2))
_moves = ()
orderings = [[(3, 4, 2, 6, 1, 5, 7, 8), (board_size - 1, board_size - 2)],
             [(8, 7, 6, 4, 2, 1, 3, 5), (2, 2)],
             [(5, 1, 8, 6, 7, 3, 4, 2), (board_size - 8, 1)],
             [(5, 1, 3, 4, 2, 6, 7, 8), (7, board_size - 3)],
             [(2, 1, 4, 3, 5, 6, 7, 8), None]
]

move_orderings = [
    [[ordered_moves[m - 1] for m in orderings[o][0]], orderings[o][1]]
    for o in range(len(orderings))]


def chess2position(chess, size=board_size):
    'Convert Algebraic chess notation to internal index format'
    chess = chess.strip().lower()
    x = ord(chess[0]) - ord('a')
    y = size - int(chess[1:])
    return x, y


def position2chess(pos, size=board_size):
    return unichr(pos[0] + ord("a")) + unichr(size - pos[1] + ord("0"))


def boardstring(board, size=board_size):
    r = range(size)
    lines = ''
    for y in r:
        lines += '\n' + '%2i ' % (size - y) + '|'.join(
            '%2i' % board[(x, y)] if board[(x, y)] else '  ' for x in r)
    lines += '\n   ' + '|'.join(
        '%2s' % unichr(x) for x in range(ord('a'), ord('a') + size))
    return lines


def real_boardstring(board, size=board_size):
    r = range(size)
    lines = ''
    for y in r:
        lines += '\n' + '%2i ' % y + '|'.join(
            '%2i' % board[(x, y)] if board[(x, y)] else '  ' for x in r)
    lines += '\n   ' + '|'.join('%2s' % x for x in range(size))
    return lines


def calc_next_possible_positions(board, current_position,
                                 board_size=board_size):
    pos_x, pos_y = current_position
    positions = [(pos_x + x, pos_y + y) for x, y in _moves
                 if 0 <= pos_x + x < board_size and 0 <= pos_y + y < board_size
                 and not board[(pos_x + x, pos_y + y)]]
    return positions


def choose_next_position(board, pos, size=board_size):
    access = []

    possible_positions = calc_next_possible_positions(board, pos,
                                                      board_size=size)
    for i in range(len(possible_positions)):
        access.append((len(
            calc_next_possible_positions(board, possible_positions[i],
                                         board_size=size)), i,
                       possible_positions[i]))

    return min(access)[2]


def warnsdoff_algorithm(start_pos, size=board_size, _debug=False):
    global _moves
    path = []
    ordering = 0
    _moves = move_orderings[ordering][0]
    board = {(x, y): 0 for x in range(size) for y in range(size)}
    move = 1
    pos = start_pos
    board[pos] = move
    path.append(position2chess(pos))

    if _debug:
        print(boardstring(board, size=size))

    for move in range(2, len(board) + 1):
        if move_orderings[ordering][1] and move_orderings[ordering][1] == pos:
            ordering += 1
            _moves = move_orderings[ordering][0]
        pos = choose_next_position(board, pos, size)

        board[pos] = move
        path.append(position2chess(pos))
        if _debug:
            print(boardstring(board, size=size))

    return board, path


if __name__ == '__main__':
    # board_size = int(input('\nboard size: '))
    # if board_size < 5:
    # print "Board is too small"
    board = {(x, y): 0 for x in range(board_size) for y in range(board_size)}
    print(boardstring(board, size=board_size))
    start = raw_input('Start position: ')
    pos = chess2position(start, board_size)
    print "pos: %s, %s" % (pos[0], pos[1])
    try:
        board_ready, path = warnsdoff_algorithm(pos, board_size, True)
        print(boardstring(board_ready, size=board_size))
        print "Knight's tour: \n%s" % path
    except:
        print "Algorithm failed for position: %i, %i" % (pos[0], pos[1])
        raise
