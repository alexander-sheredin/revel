__author__ = 'Alexander Sheredin'

import unittest

from main import board_size


class TestKnightsTour(unittest.TestCase):
    def test_warnsdoff_algorithm(self):
        from main import warnsdoff_algorithm

        board = {(x, y): 0 for x in range(board_size)
                 for y in range(board_size)}
        for pos in board:
            try:
                warnsdoff_algorithm(pos, board_size)
            except:
                self.fail(
                    "Algorithm failed for position: %i, %i" % (pos[0], pos[1]))
                raise

    """
    Baseline was not working for some positions because it does not use
    tie-breaking algorithm
    """

    def test_baseline(self):
        from baseline import knights_tour

        board = {(x, y): 0 for x in range(board_size)
                 for y in range(board_size)}
        for pos in board:
            try:
                knights_tour(pos, board_size)
            except:
                self.fail(
                    "Algorithm failed for position: %i, %i" % (pos[0], pos[1]))


if __name__ == '__main__':
    suite = unittest.TestLoader().loadTestsFromTestCase(TestKnightsTour)
    unittest.TextTestRunner(verbosity=2).run(suite)