# Knight's Tour 

## The problem description. ##
The problem of finding a closed knight's tour. A knight's tour is a sequence of moves of a knight on a chessboard such that the knight visits every square only once. 
Input: 
- Chessboard 8x8
- User sets starting point of the Knight

Desired output:
- Provide the Knight's tour path 

## Solution: ##
I learned about the problem and found that the optimal (linear time) way of finding the Knight's Tour is the one using Warnsdorf's rule.
Since the problem is well known I decided not to reinvent the bicycle, but look for existing implementations.
I found one and used it as a baseline:
http://rosettacode.org/wiki/Knight's_tour#Python

The problems with this one:
1. There was a room for optimizations
2. It did not work for some starting positions, because it did not have correct tiebreaking method.

So, what I added to the algorithm was Squirrel's algorithm, well descibed here:
https://www.cs.cmu.edu/~sganzfri/Knights_REU04.pdf

Run the program:
$ python knights_tour/main.py

Run the tests (every position of chessboard 8x8 is tested for both baseline (fails) and my algorithm):
$ python knights_tour/test_tours.py 
test_baseline (__main__.TestKnightsTour) ... FAIL
test_warnsdoff_algorithm (__main__.TestKnightsTour) ... ok

======================================================================
FAIL: test_baseline (__main__.TestKnightsTour)
----------------------------------------------------------------------
Traceback (most recent call last):
  File "knights_tour/test_tours.py", line 37, in test_baseline
    "Algorithm failed for position: %i, %i" % (pos[0], pos[1]))
AssertionError: Algorithm failed for position: 3, 0

----------------------------------------------------------------------
Ran 2 tests in 0.237s

FAILED (failures=1)


# Lowest Common Ancestor 

## The problem description. ##
The problem is known is Lowest Common Ancestor
http://en.wikipedia.org/wiki/Lowest_common_ancestor

Input: 
- Directed Acyclic Graph
- User selects two nodes

Desired output:
- algorithm provides lowest common ancestor

## Solution: ##
Since this is a well known mathematical problem, I, again, did not reinvent a bycicle... 
One of the best algorithms for finding LCA is the one presented by Michael Bender and Martin Farach-Colton (2000).
http://www.cs.sunysb.edu/~bender/pub/lca.ps

0(n) time preprocessing and 0(1) time query
The algorythm implementation in Python can be easily found:
https://www.ics.uci.edu/~eppstein/PADS/LCA.py

I would not like to try to improve it unless there are clear requirements for that.
So, I decided not to modify algorithm but to add visualization.

client part:
html5 + javascript + vis.js library:
Provides web user interface: 
- allows to quickly generate random directed acyclic graph + add/delete nodes
- every time graph is changed the "network" message is sent to server with complete hierarchy.
- once graph is created and client is connected to server, user can select 2 or more nodes, then application will highlight LCA for them (calculated by server).

- NOTE: there should be only one consistent graph tree, otherwise server will not provide LCA

server part:
Python, Tornado web server:
The server accepts client messages "network" and "lca". For "lca" messages server responds to client, providing the calculated lca.


To run the application:
1. $ python lca/server/server.py
2. open lca/client/index.html in the browser
3. press "Connect"
4. enter number of nodes and press "Generate"
5. select mutiple nodes using long "click"