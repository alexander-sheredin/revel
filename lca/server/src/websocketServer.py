# import the libraries
import json

import tornado.web
import tornado.websocket
import tornado.ioloop
from third_party.lca import LCA, LogarithmicRangeMin


WEB_SOCKET_SERVER_PORT = 50505

"""
This is our WebSocketHandler - it handles the messages
from the tornado server. Parses messages, and
encodes responses.
All processing login is delegated to serverController
and callbackManager.
"""


class WebSocketHandler(tornado.websocket.WebSocketHandler):
    lca = None

    def write_message(self, *args, **kw):
        try:
            tornado.websocket.WebSocketHandler.write_message(self, *args, **kw)
        except:
            import traceback
            traceback.print_exc()

    def check_origin(self, origin):
        return True

    def open(self):
        # Say Hello =)
        print "CLIENT CONNECTED", hex(id(self))
        self.write_message("You are connected\n")

        # Define Handlers
        self.handle_message = {
            "network": self.handle_network_msg,
            "lca": self.handle_lca_msg,
        }

    # the client sent the message
    def on_message(self, message):
        print hex(id(self)), message

        # Parse JSON
        try:
            m = json.loads(message)
            self.handle_message[m["type"]](m["content"])
        except:
            import traceback

            traceback.print_exc()

    # client disconnected
    def on_close(self):
        print "Client disconnected", hex(id(self))

    def handle_network_msg(self, content):
        print "Network message received: %s" % content
        network = {}
        for entry in content:
            network[entry["from"]] = entry["to"]
        WebSocketHandler.lca = LCA(network, LogarithmicRangeMin)


    def handle_lca_msg(self, content):
        arg = tuple(content)
        print "LCA message received: %s" % (arg,)
        lca = None
        # try:
        lca = WebSocketHandler.lca(*arg)
        print "LCA %s: %s" % ((arg,), lca)
        #except:
        #    pass
        r = {"type": "lca", "lca": lca}
        self.write_message(json.dumps(r))


class WSServer:
    def __init__(self):
        self.application = tornado.web.Application([
            (r"/", WebSocketHandler),
        ])

    def start(self):
        self.application.listen(WEB_SOCKET_SERVER_PORT)
        tornado.ioloop.IOLoop.instance().start()
