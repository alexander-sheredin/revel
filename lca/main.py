__author__ = 'Alexander Sheredin'

import unittest

from lca.server.third_party.lca import LCATest


if __name__ == "__main__":
    suite = unittest.TestLoader().loadTestsFromTestCase(LCATest)
    unittest.TextTestRunner(verbosity=2).run(suite)


