/**
 * Alexander Sheredin
 */

var networkData = {
    last_id: 0,
    get_next_id: function () {
        do {
            this.last_id++;
        }
        while (nodes.get(this.last_id) != null);
        return this.last_id.toString();
    },
    nodes: new vis.DataSet(),
    edges: new vis.DataSet(),
    lca_node: null,
    create_graph: function () {
        // Generates random graph
        // The function is based on vis examples

        console.log("create_graph()");
        var nodeCount = $("#node-count").val().toString();
        var connectionCount = [];
        nodes = new vis.DataSet();
        edges = new vis.DataSet();

        for (var i = 0; i < nodeCount; i++) {
            nodes.add({
                id: String(i),
                label: String(i),
                allowedToMoveX: true,
                allowedToMoveY: true,
                shape: "circle"
            });

            connectionCount[i] = 0;

            // create edges in a scale-free-network way
            if (i == 1) {
                var from = i;
                var to = 0;
                edges.add({
                    from: String(from),
                    to: String(to)
                });
                connectionCount[from]++;
                connectionCount[to]++;
            }
            else if (i > 1) {
                var conn = edges.length * 2;
                var rand = Math.floor(Math.random() * conn);
                var cum = 0;
                var j = 0;
                while (j < connectionCount.length && cum < rand) {
                    cum += connectionCount[j];
                    j++;
                }

                var from = i;
                var to = j;
                edges.add({
                    from: String(from),
                    to: String(to)
                });
                connectionCount[from]++;
                connectionCount[to]++;
            }
        }
        networkView.show_network();
        send_network(edges.get({fields: ['from', 'to']}));
        this.nodes = nodes;
        this.edges = edges;
        this.last_id = nodeCount - 1;
    },
    lca_refresh: function (selected_nodes) {
        if (this.lca_node) {
            this.nodes.update({
                id: this.lca_node.id,
                color: networkView.node_color(NODE_COLOR_DEFAULT),
                borderWidth: 1
            });
            this.lca_node = null;
            status("Use long press to select multiple nodes.");
        }

        if (selected_nodes.length > 1) {
            request_lca(selected_nodes);
        }
    },
    set_lca: function (id) {
        this.lca_node = networkData.nodes.get(id);
        nodes.update({
            id: id,
            color: networkView.node_color(NODE_COLOR_LCA),
            borderWidth: 2
        });
    }

};

var networkView = {
    network: null,
    options: jQuery.extend({}, default_options),
    show_network: function () {
        this.options.hierarchicalLayout.enabled = true;
        this.draw();
        this.network.storePositions();
        this.options.hierarchicalLayout.enabled = false;
        this.draw();
    },
    change_dynamic: function (l_cb) {
        if (l_cb.checked) {
            this.options.stabilize = false;
            this.options.freezeForStabilization = true;
            this.options.physics = {
                barnesHut: {}
            };
        }
        else {
            this.options.stabilize = true;
            this.options.freezeForStabilization = true;
            this.network.storePositions();
            this.options.physics = {
                barnesHut: {gravitationalConstant: 0, springConstant: 0, centralGravity: 0}
            };
        }
        this.draw();
        this.network.storePositions();
    },
    change_layout: function (l_cb) {
        this.options.hierarchicalLayout.enabled = (l_cb.checked) ? true : false;
        this.draw();
        this.network.storePositions();
    },
    draw: function () {
        var menu = document.getElementById('mymenu');
        var container = document.getElementById('mynetwork');
        var data = {
            nodes: nodes,
            edges: edges
        };
        console.log("draw() networkData:");
        console.log(data);

        this.network = new vis.Network(container, data, this.options);
        // add event listeners
        this.network.on("select", function (params) {
            networkData.lca_refresh(params.nodes);
            document.getElementById('selection').innerHTML = 'Selection: ' + params.nodes;
        });
        status("Use long press to select multiple nodes.");
    },
    node_color: function (clr) {
        color = {
            border: clr,
            background: 'white',
            highlight: {
                border: NODE_COLOR_HIGHLIGHT,
                background: 'white'
            },
            hover: {
                border: 'rgb(90, 180, 240)',
                background: 'rgb(240, 240, 255)'
            }
        };
        return color;
    }
};

// User Interface stuff
function handle_connect_btn() {
    if (!socket || socket.readyState != 1) {
        connect();
        console.log("socket not ready. connecting...");
    }
    else {
        disconnect();
        console.log("socket not ready. connecting...");
    }
}

