/**
 * Created by Alexander Sheredin
 */

// Node colors
NODE_COLOR_DEFAULT = "rgb(67, 164, 213)";
NODE_COLOR_LCA = "rgb(70, 139, 65)";
NODE_COLOR_HIGHLIGHT = "rgb(250, 30, 50)";
NODE_COLOR_HOVER = 'rgb(90, 180, 240)';

var default_options = {
    locales: {
        mylocale: {
            edit: 'Edit',
            del: 'Delete selected',
            back: 'Back',
            addNode: 'Add Node',
            addEdge: 'Add Edge',
            editNode: 'Edit Node',
            editEdge: 'Edit Edge',
            addDescription: 'Click in an empty space to place a new node.',
            edgeDescription: 'Click on a node and drag the edge to another node to connect them.',
            editEdgeDescription: 'Click on the control points and drag them to a node to connect to it.',
            createEdgeError: 'Cannot link edges to a cluster.',
            deleteClusterError: 'Clusters cannot be deleted.'
        }
    },
    locale: 'mylocale',
    hover: true,
    clustering: true,

    navigation: true,
    keyboard: true,
    dragNodes: true,
    dragNetwork: true,


    tooltip: {
        delay: 300,
        fontColor: "black",
        fontSize: 14, // px
        fontFace: 'Roboto',
        color: {
            border: "#666",
            background: "#FFFFC6"
        }
    },

    nodes: {
        title: undefined,
        color: {
            border: NODE_COLOR_DEFAULT,
            background: 'white',
            highlight: {
                border: NODE_COLOR_HIGHLIGHT,
                background: 'white'
            },
            hover: {
                border: 'rgb(90, 180, 240)',
                background: 'rgb(240, 240, 255)'
            }
        },
        allowedToMoveX: true,
        allowedToMoveY: true
    },
    edges: {
        length: 50,
        style: "arrow",
        color: {
            color: NODE_COLOR_DEFAULT,
            highlight: NODE_COLOR_DEFAULT,
            hover: NODE_COLOR_DEFAULT
        }
    },

    width: "1220px",
    height: "550px",

    stabilize: false,
    hierarchicalLayout: { enabled: true, layout: "direction", direction: "DU" },
    freezeForStabilization: true,

    physics: {
        barnesHut: {gravitationalConstant: 0, springConstant: 0, centralGravity: 0}
    },
    smoothCurves: false,

    dataManipulation: true,
    onAdd: function (data, callback) {
        var id = networkData.get_next_id();
        nodes.add({
            id: id,
            label: id,
            allowedToMoveX: true,
            allowedToMoveY: true,
            shape: "circle",
            x: data.x,
            y: data.y
        });

    },
    onConnect: function (data, callback) {
        if (data.from == data.to) {
            return;
        }
        else {
            var new_data = {
                from: data.from,
                to: data.to
            };

            if (networkData.edges.get({ filter: function (item) {
                return(((item.from == data.from)) ||
                    ((item.from == data.to) && (item.to == data.from)));
            }}).length) {
                console.log(networkData.edges.get(new_data));
                // multiple connection between the same nodes are forbidden
                // multiple parents are forbidden as well
                return;
            }
            else {
                networkData.edges.add(new_data);
                send_network(networkData.edges.get({fields: ['from', 'to']}));
            }

        }


    },
    onDelete: function (data, callback) {
        callback(data);  // call the callback to delete the objects.
        send_network(networkData.edges.get({fields: ['from', 'to']}));
    }
};


