var socket;

function status(msg) {
    var s = document.getElementById('status');
    if (s) {
        s.textContent = msg;
    } else {
        console.log(msg);
    }
}


function connect() {
    var url = document.getElementById('url');
    if (!url) {
        url = "ws://127.0.0.1:50505";
    }
    else if (url.value == "") {
        url = "ws://127.0.0.1:50505";
    } else {
        url = url.value;
    }
    socket = new WebSocket(url);
    status('Connecting to "' + url + '"...');

    socket.onopen = function (event) {
        status('Connected to "' + socket.url + '".');
    };

    socket.onmessage = function (event) {
        console.log('\nRCVD: ' + event.data);
        try {
            response = JSON.parse(event.data);
        }
        catch (err) {
            // Unable to parse JSON
            console.log("\nInvalid JSON received from server: " + event.data);
            var textArea = document.getElementById('ConsoleOutputArea');
            if (textArea) {
                textArea.scrollTop = textArea.scrollHeight;
            }
            return;
        }

        console.log("\n" + response.type + " response received");
        switch (response.type) {
            case "lca":
                console.log('LCA RSP RCVD: ' + event.data);
                handle_lca_response(response.lca);
                break;
            default:
                break;
        }
    };

    socket.onclose = function (event) {
        status('Disconnected.');
        $("#connect-btn").find(".btn-text").textContent = "Connect";
    }
}


function disconnect() {
    if (socket) {
        status('Disconnecting.');
        socket.close();
    }
}

function send() {
    socket.send(document.getElementById('text').value);
    document.getElementById('text').value = "";
}

function send_data(data) {
    socket.send(data);
}

function send_network(edges) {
    if (!socket || socket.readyState != 1) {
        console.log("send_network(): not connected")
        return;
    }
    var msg = {
        "type": "network",
        "content": edges
    };
    send_data(JSON.stringify(msg));
}

function request_lca(nodes) {
    if (!socket || socket.readyState != 1) {
        console.log("send_lca(): not connected")
        return;
    }
    status("Requesting LCA for " + nodes);
    var msg = {
        "type": "lca",
        "content": nodes
    };
    send_data(JSON.stringify(msg));
}

function handle_lca_response(lca) {
    if (lca) {
        status("LCA: " + lca);
        networkData.set_lca(lca);

    }
    else {
        status("Could not determine LCA");
    }

}
